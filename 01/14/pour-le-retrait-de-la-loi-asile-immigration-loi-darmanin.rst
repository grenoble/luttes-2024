.. index::
   pair: CISEM ; #DesobeissanceCivile

.. _cisem_2024_01_14:

========================================================================================================================
2024-01-14 **Pour le retrait de la loi asile immigration (loi darmanin)** par la CISEM #DesobeissanceCivile  
========================================================================================================================

- https://21janvier.fr/
- https://antiracisme-solidarite.org/agenda/
- https://www.humanite.fr/politique/ils-disent-non-a-la-loi-immigration/patrick-baudouin-president-de-la-ldh-cette-loi-est-ce-quon-peut-faire-de-pire-en-matiere-de-droits-des-etrangers


#PersonneNestIllegal #SolidariteAntiraciste SolidariteAntifasciste #ContreLaLoiDarmanin
#DesobeissanceCivile #CISEM #14Janvier #14Janvier 2024 #21Janvier  #21Janvier2024
#RetraitLoiImmigration #Solidarite

.. tags:: DesobeissanceCivile, Retrait Loi Immigration


.. figure:: images/solidarite.webp
   :width: 800
   
   #Solidarite

.. figure:: images/manif_2024_01_14.webp
   :width: 800
   
   #PersonneNestIllegal #SolidariteAntiraciste SolidariteAntifasciste #ContreLaLoiDarmanin

|cisem| Pour le retrait de la loi asile immigration (loi darmanin) manifestation le dimanche 14 janvier, 14h30 départ place Victor Hugo 
==============================================================================================================================================

:download:`Tract Pour le retrait de la loi asile immigration (loi darmanin) <pdfs/manifestation_grenoble_2024_01_14.pdf>`

**La loi asile immigration marque un tournant que nos collectifs, associations,
syndicats, organisations ne peuvent accepter**. 

**Elle reprend de nombreuses idées de l’extrême droite** comme la préférence 
nationale et aura des conséquences terribles sur la vie des millions 
d’habitant.e.s étrangère-es ou pas sur le sol français.  

Il s’agit de la loi la plus régressive depuis 40 ans. 

Cette loi raciste et xénophobe restreint le droit au séjour, accentue
considérablement la répression, s’attaque au droit d’asile, au droit du
sol, aux étrangers malades, aux étudiants non européens, au regroupement
familial. 

L’attaque contre l’hébergement d’urgence et le durcissement de l’accès 
aux prestations sociales dont les allocations familiales et les aides 
aux logements, vont jeter des familles à la rue ou dans les bras
de marchands de sommeil et particulièrement les femmes migrantes. 

Cette loi va précariser davantage les travailleuses et travailleurs, 
les lycéen.nes, les étudiant.es avec ou sans-papiers.  

**L’arbitraire préfectoral est encore renforcé**, refoulement aux frontières, 
délivrance systématique des OQTF et IRTF et allongement de leur durée, 
notamment pour les travailleuses et les travailleurs. 

Cette loi s’attaque aux libertés publiques, bafoue les droits fondamentaux 
tels que le droit d’asile, réinstaure la double peine, dans ce pays qui 
prétend défendre les valeurs d’égalité entre toutes et tous. 

Nous exigeons donc le retrait de cette loi.

|cisem| Appel de la CISEM
==============================

Nous appelons : 

- à soutenir toutes les luttes pour la régularisation des sans-papiers,
- à obtenir le retrait de cette loi en multipliant les actions de solidarité et
  en faisant œuvre de **désobéissance civile** 
- à **manifester massivement** sur tout le territoire le **dimanche 14 janvier 2024**, 
  pour empêcher que cette loi ne voie le jour, combattre tous les racismes, 
  la xénophobie et défendre une politique migratoire d’accueil et de solidarité.  
  
Au vu de la situation sociale et politique, nous appelons également à la 
manifestation du **21 janvier 2024** et à toutes les actions qui suivront 
pour le retrait de cette loi 

La CiSEM ( Coordination isèroise de solidarité avec les étranger.e.s migrant.e.s)


|cisem|  La plateforme CISEM
=================================

:download:`La plateforme CISEM <pdfs/plateforme_cisem.pdf>`

La CISEM est ouverte aux associations, syndicats, organisations politiques, 
réseaux et collectifs qui sont en accord avec cette plateforme.

La CISEM lutte CONTRE

- toutes les discriminations
- tous les racismes en défendant avec les premiers concerné.es les droits individuels et collectifs

La CISEM lutte POUR :

- la liberté de circulation et d’installation des personnes
- la pleine reconnaissance du droit d’asile
- la régularisation de toutes et tous les sans papiers
- l’égalité de tous/toutes à l’accès aux droits fondamentaux 
  
  - à l’éducation,
  - à la santé, 
  - au travail, 
  - au logement…
  
- l’ouverture des frontières
- la fermeture des centres de rétention administrative (CRA)
- le droit de vote et d'éligibilité de tous les résident.es étranger.es
- l’application de la convention internationale des droits de l’enfant


La CISEM regroupe à ce jour les organisations suivantes 
-----------------------------------------------------------

Associations et réseaux: 
++++++++++++++++++++++++++++

- ACIP ASADO (Association pour la Coopération Inter-Peuples & Action
  de Solidarité avec les peuples d'Amérique latine), 
- APARDAP (association de parrainage républicain des demandeurs d’asile 
  et de protection), 
- ATTAC 38, 
- CSRA (comité de soutien aux réfugiés algériens), 
- Collectif hébergement logement, 
- DAL 38 (droit au logement), 
- LDH (ligue des droits de l’Homme) Grenoble et Isère, 
- LIFPL (ligue internationale des femmes pour la paix et le liberté),
- RLF (réseau de lutte contre le fascisme), 
- RESF (réseau éducation sans frontière), 
- RUSF (réseau universitaire sans frontière)

Syndicats
++++++++++++

- FSU, 
- Solidaires Isère 38, 
- Syndicat multiprofessionnel des travailleurs sans papiers CGT, CGT 38


Organisations politiques 
+++++++++++++++++++++++++++

- Ensemble Isère!, 
- EELV Isère, 
- LFI, 
- Go citoyenneté, 
- NPA, 
- PCF, 
- PCOF,
- UCL

Grenoble, mai 2023

