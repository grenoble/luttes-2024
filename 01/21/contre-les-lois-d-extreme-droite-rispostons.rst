.. index::
   pair: Manfestation; Contre les lois d'extrême droite ripostons! (2024-01-21)

.. -grenoble_2024_01_21:

================================================================
2024-01-21 **Contre les lois d'extrême droite ripostons !**
================================================================

- https://21janvier.fr/

#Grenoble #PersonneNestIllegal #SolidariteAntiraciste SolidariteAntifasciste #ContreLaLoiDarmanin
#DesobeissanceCivile #CISEM #14Janvier #14Janvier 2024 #21Janvier  #21Janvier2024
#RetraitLoiImmigration #Solidarite


Contre les lois d'extrême droite ripostons!

Pour l'accueil et la solidarité, retrait de la loi asile immigation

21 janvier 2024 Manifestation à Grenoble 14h place Félix Poulat

Tracts de la CGT
======================

- https://r.newsletter.cgt.fr/mk/mr/sh/1t6AVsd2XFnIGNVWKv9LliXJA7U0i9/toz1PHh-8YFR

.. figure:: images/cgt_visuels.webp

Tract de soutien aux livreurs
=================================

:download:`Tract de soutien aux livreurs <pdfs/soutien_aux_livreurs_2024_01_21.pdf>`


Tract migrants en Isère
=================================

:download:`Tract de soutien aux livreurs <pdfs/tract_migrants_2024_01_21.pdf>`


Tract des syndicats
=================================

:download:`Tract de soutien aux livreurs <pdfs/Tract_syndicats_2024_01_21.pdf>`

