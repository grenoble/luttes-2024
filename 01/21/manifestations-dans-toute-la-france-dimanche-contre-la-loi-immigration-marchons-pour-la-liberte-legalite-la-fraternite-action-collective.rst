.. index::
   pair: Gisti; Manifestations dans toute la France dimanche contre la loi immigration : «Marchons pour la Liberté, l’égalité, la fraternité» [Action collective] (2024-01-21)

.. _gisti_2024_01_21:

====================================================================================================================================================================
2024-01-21 **Manifestations dans toute la France dimanche contre la loi immigration : «Marchons pour la Liberté, l’égalité, la fraternité» [Action collective]**
====================================================================================================================================================================

Action collective Contre la loi « asile-immigration »
Marchons pour la Liberté, l’égalité, la fraternité Manifestations dans toute la France
dimanche 21 janvier 2024


REMARQUE : Le Gisti n’est pas signataire de cette tribune de personnalités
qui a été publiée entre le 7 et le 10 janvier 2024 par L’Humanité et
Mediapart, mais, comme il était indiqué dans son communiqué du 11 janvier
« Mobilisons-nous pour le retrait de la loi immigration ! », le Gisti
appelle à se joindre dimanche à cette mobilisation qui se déroulera dans toute la France.

Tous les lieux et horaires des manifestations → https://21janvier.fr

Paris : départ 14h du Trocadéro en direction des Invalides.

Dans notre diversité d’idées, d’engagements, de professions, nous exprimons
notre grande inquiétude après le vote de la loi dite « asile-immigration ».

C’est un tournant dangereux dans l’histoire de notre République.

D’abord parce que cette loi ne répond pas aux causes de l’exil forcé
d’hommes, de femmes, d’enfants fuyant les guerres ou le réchauffement
climatique, ni aux défis de l’accueil dans la dignité, ni au défi de la
définition d’une politique digne et humaine d’intégration.

Ensuite, parce qu’elle a été rédigée sous la dictée des marchands de haine
qui rêvent d’imposer à la France leur projet de « préférence nationale ».

Elle torpille les piliers porteurs de notre pacte républicain, hérité du
Conseil national de la Résistance.

Elle s’attaque ainsi au droit du sol autant qu’aux droits fondamentaux
proclamés par la Constitution : au travail, à l’éducation, au logement,
à la santé… Ce faisant, tous et toutes, Français autant qu’étrangers,
nous nous trouvons menacés.

Victor Hugo écrivait : « Étouffez toutes les haines, éloignez tous les
ressentiments, soyez unis, vous serez invincibles. »

Soucieux de rassemblement et de solidarité plutôt que de division sans
fin de notre société, nous demandons au Président de la République de ne
pas promulguer cette loi.

Le dimanche 21 janvier nous appelons à manifester dans notre diversité
notre attachement à la devise de la République : « Liberté, égalité, fraternité. »

janvier 2024



Les 201 signataires
==========================

::

    Serge Abiteboul, informaticien, membre de l’Académie des sciences
    Emile Ackermann, rabbin
    Syrine Aït Si Ali, présidente de la FIDL
    Fleur Albert, réalisatrice, documentariste
    Christophe Alévêque, comédien
    Anne Alvaro, actrice
    Hakim Amokrane, musicien
    Mouss Amokrane, musicien
    Hortense Archambault, responsable théâtrale
    Pierre Arditi, comédien
    Swann Arlaud, acteur
    Ariane Ascaride, comédienne
    Ana Azaria, présidente de Femmes Égalité
    Josiane Balasko, comédienne
    Étienne Balibar, philosophe
    Christian Baudelot, sociologue
    Patrick Baudouin, président de la LDH
    Thierry Beaudet, président du Conseil économique social et environnemental
    Karim Benaïssa, recteur de la mosquée de Créteil, président du RAM 94
    Farid Bennaï, président du Front uni des immigrations et des quartiers populaires
    Lucie Berelowitsch, metteuse en scène, directrice du Préau CDN Normandie-Vire
    Marlise Bété, actrice, scénariste, réalisatrice
    Laurent Binet, écrivain
    Sophie Binet, secrétaire générale de la CGT
    David Bobée, metteur enscène
    Manuel Bompard, député
    Pascal Bonitzer, réalisateur
    Mosco Levi Boucault, réalisateur
    Patrick Boucheron, historien, professeur au Collège de France
    François Bourdillon, médecin de santé publique
    Rachida Brakni, actrice
    Stéphane Braunschweig, directeur de l’Odéon
    Michel Broué, mathématicien
    Émilie Capliez, codirectrice Comédie de Colmar CDN
    Antoine Chambert-Loir, mathématicien
    Patrick Chamoiseau, écrivain
    Noëlle Châtelet, écrivaine
    Éric Chenut, président de la Mutualité française
    Margot Chevalier, co-présidente de Chrétiens en monde rural
    Malik Chibane, réalisateur
    Hervé Chneiweiss, biologiste, directeur de recherches au CNRS
    Gilles Cohen-Tannoudji, physicien
    Philippe Corcuff, professeur de science politiques à Lyon
    Karine Cornilly, co-présidente de l’Action catholique ouvrière
    Mathieu Cruciani, codirecteur Comédie de Colmar CDN
    Chloé Dabert, metteuse en scène, Comédie de Reims
    Fanny De Chaillé, metteuse en scène, Théâtre national de Bordeaux
    Julie Deliquet, metteuse en scène, Théâtre Gérard Philipe
    Christian Delorme, prêtre, co-initiateur de Marche pour l’égalité de 1983
    Marc Deluzet, président de l’Action catholique des milieux indépendants
    Philippe Descola, professeur émérite au Collège de France
    Véronique Devise, présidente du Secours Catholique
    Martial Di Fonzo Bo, metteur en scène, directeur du Quai CDN Angers
    Fatou Diome, écrivaine
    Alice Diop, cinéaste
    David Diop, professeur des universités et écrivain
    Toumi Djaïdja, co-initiateur et symbole de la Marche pour l’égalité de 1983
    Nasser Djemaï, metteur en scène, Théâtre des Quartiers d’Ivry, CDN Val-de-Marne
    Cécile Duflot, directrice d’Oxfam France
    François Dunoyer, comédien
    Annie Duperey, comédienne
    Simon Duteil, co-porte-parole de Solidaires
    Nadia El Fani, réalisatrice
    Marie-France Eprinchard, présidente d’Emmaüs solidarité
    Laurent Escure, secrétaire général de l’UNSA
    Philippe Faucon, réalisateur
    Olivier Faure, député
    Gilles Finchelstein, secrétaire général de la Fondation Jean Jaurès
    Marina Foïs, comédienne
    Nathalie Garraud, codirectrice Théâtre 13 Vents Montpellier
    Fabien Gay, directeur de l’Humanité
    Julie Gayet, actrice
    Caroline Glorion, réalisatrice
    Éléa Gobbé-Mévellec, réalisatrice
    Daniel Goldberg, président de l’Uniopss
    Emmanuelle Gourvitch, metteuse en scène, présidente du Synavi
    Marie-Aleth Grard, présidente d’ATD Quart Monde
    André Grimaldi, professeur émérite de médecine
    Anouk Grinberg, comédienne et peintre
    Joanna Grudzinska, réalisatrice
    Murielle Guilbert, co-porte-parole de Solidaires
    Alain Guiraudie, cinéaste
    Hanja Hamidi, présidente de l’UNEF
    Benoît Hamon, directeur général de SINGA
    Philippe Hardouin, ex-président d’En Commun
    Cédric Herrou, responsable Emmaüs Roya
    Albert Herszkowicz, porte-parole du RAAR
    Clotilde Hesme, comédienne
    Nancy Huston, écrivaine
    Jonathan Israël, réalisateur
    Kaori Ito, directrice TJP CDN Strasbourg-Grand-Est
    Eddie Jacquemart, président de la Confédération nationale du logement
    Jok’Air, musicien
    Pierre Joliot, biologiste, membre de l’Académie des sciences
    Élisabeth Jonniaux, réalisatrice
    Jean Jouzel, paléoclimatologue, co-lauréat du Prix Nobel de la paix 2007 avec le GIEC
    Karim Kacel, chanteur
    Sam Karmann, comédien, réalisateur
    Issam Krimi, musicien, compositeur
    Judith Krivine, présidente du Syndicat des avocats de France
    Thomas Lacoste, auteur et réalisateur
    Guillaume Lacroix, conseiller régional
    Leslie Lagier, cinéaste
    Hélène Langevin-Joliot, physicienne
    Sébastien Laudenbach, cinéaste
    Lionel Lecerf, co-président de l’Action catholique ouvrière
    Messica Lee Fou, Espace Bernard-Marie Koltès Metz
    Cyrille Legrix, metteur en scène, président du SNMS
    Marylise Léon, secrétaire générale de la CFDT
    Arrigo Lessana, chirurgien du cœur, écrivain
    Danièle Linhart, sociologue
    Émilie Loizeau, chanteuse, musicienne
    Christine Malard, directrice Théâtre Jean Lurçat Aubusson
    Anna Marmiesse, scénariste et réalisatrice
    Corinne Masiero, comédienne
    Henry Masson, président de la Cimade
    Valérie Masson-Delmotte, paléoclimatologue, coprésidente du GIEC 2015-2023
    Joris Mathieu, metteur en scène, CDN Lyon
    Nicolas Mathieu, écrivain
    Murielle Mayette-Holtz, directrice CDN Nice Côte d’Azur
    Dominique Meda, professeur d’université Paris-Dauphine
    Guillaume Meurice, humoriste
    Perrine Michel, réalisatrice
    Jean-Pierre Mignard, avocat
    Maria-Carmela Mini, directrice de Latitudes contemporaines
    Sandrine Mini, directrice TMS scène nationale Archipel de Thau
    Dominik Moll, réalisateur
    Gérard Mordillat, écrivain et cinéastes
    François Morel, acteur, humoriste
    Arthur Nauzyciel, acteur et metteur en scène
    Maëlle Nizan, présidente de la FAGE
    Gérard Noiriel, directeur d’études à l’EHESS
    Bridget O’Driscoll, réalisatrice
    Erik Orsenna, écrivain
    Céline Pauthe, metteuse en scène, CDN Besançon Franche-Comté
    Antoine Pelissolo, professeur de psychiatrie
    Willy Pelletier, collectif Coudes à Coudes
    Patrick Pelloux, urgentiste
    Aude Pépin, actrice, scénariste, réalisatrice
    Gilles Perret, réalisateur
    Ella Perrier, directrice adjointe CDN Nice Côte d’Azur
    Michelle Perrot, historienne, professeur émérite des universités
    Ernest Pignon-Ernest, plasticien
    Edwy Plenel, cofondateur de Mediapart
    Maëlle Poésy, metteuse en scène, directrice CDN Dijon Bourgogne
    Francesca Poloniato, directrice ZEF Marseille
    Claude Ponti, illustrateur et dessinateur jeunesse
    Alexia Portal, cinéaste
    Alain Prochiantz, professeur émérite au Collège de France
    Olivier Rabourdin, acteur
    Robin Renucci, acteur et réalisateur
    Kim Reuflet, présidente du Syndicat de la magistrature
    Jean-Michel Ribes, dramaturge
    Chantal Richard, réalisatrice
    Cécile Rilhac, députée
    Christophe Robert, délégué général de la Fondation Abbé Pierre
    Sébastien Roché, sociologue
    Pierre Rosanvallon, professeur émérite au Collège de France
    Élisabeth Roudinesco, historienne et psychanalyste
    Michel Rousseau, coprésident de Tous Migrants
    Fabien Roussel, député
    Olivier Saccomaro, codirecteur Théâtre 13 Vents Montpellier
    Jérôme Saddier, président d’ESS France
    Ludivine Sagnier, actrice
    Latifa Saïd, cinéaste
    Marcela Saïd, cinéaste
    Benjamin Saint-Huile, député
    Thomas Salvador, réalisateur
    Lydie Salvayre, écrivaine
    François Sauterey, coprésident du MRAP
    Sylvie Sema Glissant, auteur, artiste plasticienne
    Pierre Serna, historien
    Gauvain Sers, chanteur
    Caroline Simpson Smith, directrice Théâtre Sénart
    Bruno Solo, comédien
    Dominique Sopo, président de SOS Racisme
    Benjamin Stora, historien
    Antoine Sueur, président d’Emmaüs France
    Benoît Teste, secrétaire général de la FSU
    Caroline Thibaut, artiste, directrice CDN Montluçon
    Samuel Thomas, président de la fédération des Maisons des potes
    Claire Thoury, présidente du Mouvement Associatif
    Marine Tondelier, conseillère régionale
    Jacques Toubon, ancien Défenseur des droits
    Chloé Tournier, directrice La Garance-Cavaillon
    Camille Trouvé, metteuse en scène, codirectrice CDN Normandie-Rouen
    Marion Truchaud, réalisatrice
    Najat Vallaud-Belkacem, présidente de France Terre d’Asile
    Alice Vaude, secrétaire nationale de l’Organisation de Solidarité Trans
    Marie-Pierre Vieu, co-présidente de la Fondation Copernic
    Jean Vigreux, historien
    Cédric Villani, mathématicien, médaille Fields
    Claude Viterbo, mathématicien
    François Vitrani, président de l’Institut du Tout-monde
    Raphaël Vulliez, collectif Jamais Sans Toit
    Uli Wittmann, écrivain
    Serge Wolikow, historien
    Youlie Yamamoto, porte-parole d’Attac
    Alice Zeniter, écrivaine
    Rebecca Zlotowski, réalisatrice
    Ruth Zylberman, écrivaine et réalisatrice

