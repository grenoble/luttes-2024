.. index::
   pair: Claude Collin ; Conférence de l'historien Claude Collin "Elles étaient toutes très jeunes. Les "filles" de la Main d'œuvre immigrée (MOI) dans la résistance iséroise

.. _collin_2024_03_26:

======================================================================================================================================================================
2024-03-26 Conférence de l'historien Claude Collin **"Elles étaient toutes très jeunes. Les "filles" de la Main d'œuvre immigrée (MOI) dans la résistance iséroise"**
======================================================================================================================================================================

- https://en.wikipedia.org/wiki/FTP-MOI
- https://www.editions-harmattan.fr/index.asp?navig=auteurs&obj=artiste&no=3593


Annonce sur gremag
====================

- https://www.gremag.fr/agenda/1276/12-80-ans-de-la-liberation-de-grenoble.htm


➡ Rejoignez-nous pour une conférence captivante de l'historien Claude Collin
"Elles étaient toutes très jeunes. Les "filles" de la Main d'œuvre immigrée
(MOI) dans la résistance iséroise" 📜

🗓️ Mardi 26 mars
🕕 18 heures
🏛️ Hôtel de Ville de Grenoble

Cette conférence sera précédée d'une projection du film "Liberté : guerilla
urbaine à Grenoble".🎥

Elles avaient entre 16 et 22 ans, elles étaient pour la plupart issues de
familles immigrées, beaucoup étaient juives.

Elles militaient dans les organisations de jeunesse de la MOI, l'Union de la
jeunesse juive, le secteur « jeunes » du Mouvement national contre le racisme...


La guerre terminée, la nation et les hommes qui la représentaient les oublièrent
et ce ne fut que longtemps après la fin du conflit que l'on commença à
reconnaître leur courage et leur mérite.

**Aujourd'hui encore, fort peu de rues de nos villes portent leurs noms**.


Emmanuel Carroz
===================

- https://www.grenoble.fr/1823-emmanuel-carroz.htm

Adjoint Mémoire, Migrations et Coopérations internationales, Europe

Enseignant et militant pour les droits humains, j’ai évolué dans le milieu
associatif (droits des personnes LGBTQI+), syndical, avant que l’on me propose
de rejoindre la liste Grenoble une ville pour tous en 2014.


.. youtube:: RxFrQWQvMo8


Réélu en 2020 sur la liste Grenoble en commun.

Jérôme Soldeville
=======================

- https://www.grenoble.fr/1845-jerome-soldeville.htm

Conseiller municipal délégué Histoire de Grenoble
Quartier de l'école Porte Saint Laurent - Quartier de l'école Bizanet

.. youtube:: dRU-X7UiGXk


Claude Collin
================

- https://www.editions-harmattan.fr/index.asp?navig=auteurs&obj=artiste&no=3593

Après des études à l'Université de Nancy (1961-1966) et deux années de coopération
en Algérie (1966-1968), je me suis installé à Grenoble où j'ai enseigné
l'allemand de 1971 à 1975.

A cette date, j'ai entrepris des études en sciences de l'information et de la
communication qui m'ont conduit à un doctorat ("La radio, instrument d'intervention
sociale et politique") en 1980.

De septembre 1977 à septembre 1984, j'ai été détaché de l'Education nationale
au Centre audiovisuel de la Villeneuve de Grenoble où j'initiais des enfants
d'âge scolaire et des adultes en formation continue à la lecture de l'image
fixe et animée, ainsi qu'à l'utilisation des techniques audiovisuelles simples
(photo, son, vidéo VHS et cinéma super 8).

D'octobre 1984 à septembre 2001, j'ai été assistant puis maître de conférences
en sciences de l'information et de la communication à l'Université Stendhal
de Grenoble.

Mes enseignements ont porté essentiellement sur l'enquête en sciences sociales,
l'histoire de la radio et de la télévision et enfin sur le traitement de
l'histoire dans les médias audiovisuels.

Je suis par ailleurs l'auteur de séries radiophoniques, de films documentaires
et de divers ouvrages sur l'histoire de l'Occupation, de la Résistance et
de l'après-guerre.


.. youtube:: -4QQdfsdYWQ

   https://www.youtube.com/watch?v=-4QQdfsdYWQ

Le film Première partie : "Liberté, guérilla urbaine à Grenoble" par Claude Collin et Denis Cugnod
======================================================================================================

- `https://cinevod.bm-grenoble.fr/video/0766C-etrangers-et-nos-frres-pourtant... <https://cinevod.bm-grenoble.fr/video/0766C-etrangers-et-nos-frres-pourtant...>`_

Francs-Tireurs et Partisans de la main d'oeuvre immigrée (FTP-MOI) à Lyon et
Grenoble : une série consacrée au rôle des étrangers dans la résistance
française en Rhône-Alpes.

- Première partie : `Liberté, guérilla urbaine à Grenoble <https://cinevod.bm-grenoble.fr/video/0766C-etrangers-et-nos-frres-pourtant...#>`_
- Deuxième partie : Carmagnole, l'insurrection de Villeurbanne.

Les images
================

.. figure:: images/1_20240326_174254_800.webp

PCF, MOE => MOI
===================


.. figure:: images/2_pcf_moe_moi.webp

Claude Collin
==============

.. figure:: images/20240326_184036_800.webp


PCF, MOI  Juifs [UJRE, UJJ, MNCR]
=======================================

- l'UJRE existe toujours et édite un mensuel "la presse nouvelle"
- le MNCR (Mouvement Nationale contre le racisme) est l'ancêtre du MRAP

.. figure:: images/20240326_184328_800.webp


Pierre Grinberg, Jean Halpern, Los Gaist, Wiktor Bardach, Irène Mendelson, Judith Ait-Hin
===============================================================================================

.. figure:: images/20240326_184735_800.webp


Judith Hait-Hin et Irène Mendelson
======================================

.. figure:: images/20240326_185842_800.webp

Hélène Waitzmann
=====================

.. figure:: images/20240326_185848_800.webp

Colette Audry et Somone Devouassoux
==========================================

.. figure:: images/20240326_190004_800.webp

Annie Becker et Jeanne Guillaud
====================================

.. figure:: images/20240326_190149_800.webp

Jeannine Kanapa
==================

.. figure:: images/20240326_190927_800.webp

Un incident criminel à la biscuiterie Brun
==============================================

.. figure:: images/20240326_190959_800.webp

Devant moi, la fille de Jeannine Kanapa
==========================================

.. figure:: images/20240326_192741_800.webp


Les villes compagnons de la Libération
============================================

- https://www.ordredelaliberation.fr/fr/les-communes-compagnon-de-la-liberation


.. figure:: images/20240326_200951_800.webp

Grenoble
-------------

- https://www.ordredelaliberation.fr/fr/grenoble

.. figure:: images/20240326_201003_800.webp

.. figure:: images/20240326_201224_800.webp


Vassieux-en-Vercors
---------------------

- https://www.ordredelaliberation.fr/fr/vassieux-en-vercors

.. figure:: images/20240326_201149_800.webp

Paris
----------

- https://www.ordredelaliberation.fr/fr/paris

.. figure:: images/20240326_201213_800.webp

Nantes
--------

- https://www.ordredelaliberation.fr/fr/nantes

.. figure:: images/20240326_201237_800.webp

L'ile de sein
----------------

- https://www.ordredelaliberation.fr/fr/ile-de-sein

.. figure:: images/20240326_201246_800.webp


Liens
=======

- https://www.cairn.info/grenoble-de-l-occupation-a-la-liberte--9782706116872.htm
