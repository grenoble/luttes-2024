.. index::
   pair: Arthur Keller le monde de demain, se préparer ici dès aujourd-hui ensemble ; 2024-06-11

.. _keller_2024_06_11:

==============================================================================================
2024-06-11 **Arthur Keller le monde de demain, se préparer ici dès aujourd-hui ensemble**
==============================================================================================

- https://www.grenoble.fr/2852-grenoble-2040.htm
- https://x.com/abkgrenoble/status/1800679199063069129
- https://www.youtube.com/@EmmaGoldman-ub5qw/videos

.. figure:: images/affiche.webp
   :width: 500


Arthur keller
=================

- https://fr.wikipedia.org/wiki/Arthur_Keller
- https://x.com/arthurkeller
- https://www.linkedin.com/in/kellerarthur/
- https://www.youtube.com/watch?v=kLzNPEjHHb8&ab_channel=NEXT-LesMondesSensibles%28byThePassengerStudio%29


.. figure:: images/arthur_keller.webp

Penseur d'alerte.
------------------------

Expert des vulnérabilités des sociétés face aux risques systémiques et des
stratégies de sécurité globale des territoires et de résilience.

sur youtube
------------------

- https://www.youtube.com/watch?v=kLzNPEjHHb8&ab_channel=NEXT-LesMondesSensibles%28byThePassengerStudio%29

Arthur Keller est ingénieur en aérospatiale de formation.

Il est aujourd’hui consultant et conférencier sur les questions d’énergie,
de climat et de transition écologique.
Il a notamment été le coordinateur de la commission environnement au parti Nouvelle Donne,
et référent du programme de Charlotte Marchandise, candidate citoyenne à
l’élection présidentielle.

Il est membre du conseil d’administration de l’association Adrastia, qui
travaille sur l’anticipation du déclin de la civilisation thermo-industrielle.

Arthur Keller est également auteur et scénariste, et explore comment le
storytelling peut être un outil de pédagogie et de mobilisation autour du
sujet de la vulnérabilité de nos sociétés, ainsi que des stratégies de résilience.

Diaporama Préparer les territoires aux risques sociétaux inédits du XXIe siècle d'Arthur Keller pour le 11 juin 2024 à Grenoble
=================================================================================================================================

- https://drive.google.com/file/d/1zKbVGofxtamjOtJKEBTC47YvnCeE7hUr/view

Préparer les territoires aux risques sociétaux inédits du XXIe siècle
-----------------------------------------------------------------------

Stratégies de mobilisation et de transformations collectives face à la
perspective de ruptures systémiques.


.. figure:: images/slide_1.webp

   https://drive.google.com/file/d/1zKbVGofxtamjOtJKEBTC47YvnCeE7hUr/view



Introduction par Antoine Back
--------------------------------

- https://www.youtube.com/@EmmaGoldman-ub5qw/videos

.. youtube:: gpwPOP-d8vQ



Arthur Keller 19h20
-----------------------------------------------------------------------------------------------


.. youtube:: VKNAxh4AVXc


Arthur Keller  20h10
------------------------

.. youtube:: su9BACvxb4w


Arthur Keller  20h34
----------------------------

.. youtube:: pdbORII8byc


Antoine Back  21h17
----------------------------

.. youtube:: su9BACvxb4w


Arthur Keller  21h37
----------------------------

.. youtube:: p_wqTo6wEPc


Arthur Keller 22h05
-------------------------------------

.. youtube:: 5Uf9kV_KQ9g


Arthur Keller et Antoine Back 22h51
-------------------------------------

.. youtube::  hfktBXUSo5I


Bibliographie
================

Les Limites À La Croissance - Edition Spéciale 50 Ans Dennis Meadows, Donella Meadows, Jorgen Randers
-------------------------------------------------------------------------------------------------------------

- https://www.librairiesindependantes.com/product/9782374253329/

::

    EAN13
        9782374253329
    Éditeur
        Rue de l'échiquier
    Date de publication
        3 mars 2022
    Collection
        ECOPOCHE
    Nombre de pages
        488
    Dimensions
        20,9 x 13 x 3,4 cm
    Poids
        450 g
    Langue
        fre

En 1972, quatre jeunes scientifiques du MIT rédigent à la demande du Club de Rome
un rapport qu'ils intitulent The Limits to Growth.
Celui-ci va choquer le monde et devenir un best-seller international.

Pour la première fois, leur recherche établit les conséquences dramatiques
d'une croissance exponentielle dans un monde fini.

En 2004, quand les auteurs reprennent leur analyse et l'enrichissent de données
accumulées durant trois décennies d'expansion sans limites, l'impact
destructeur des activités humaines sur les processus naturels les conforte
définitivement dans leur raisonnement.

En 2012, à l'occasion de la traduction française de cette dernière version,
Dennis Meadows déclare : « Il y aura plus de changements – sociaux, économiques
et politiques – dans les vingt ans à venir que durant le siècle passé. »

En 2022, que nous reste-t-il à envisager ?


La guerre des métaux rares
--------------------------------

- https://www.librairiesindependantes.com/product/9791020924759/


::

    EAN13
        9791020924759
    Éditeur
        Éditions Les Liens qui libèrent
    Date de publication
        27 septembre 2023
    Collection
        L L L
    Nombre de pages
        352
    Dimensions
        17,5 x 10,9 x 1,6 cm
    Poids
        212 g
    Langue
        fre

Transition énergétique, révolution numérique...

Politiques, médias et industriels nous promettent un monde enfin affranchi
du pétrole, des pollutions, des pénuries et des tensions militaires.

Cet ouvrage, fruit de six années d'enquête, nous montre qu'il n'en est rien !

En nous émancipant des énergies fossiles, nous sécrétons en réalité une nouvelle
dépendance : celle aux métaux rares.

À rebours des discours dominants, c'est dès lors une contre-histoire de la
transition énergétique que ce livre révèle.

Cette nouvelle édition augmentée tient compte de l'abondante actualité liée
aux métaux rares, et est enrichie d'une préface inédite de l'auteur.


Climat, Crises, Le Plan De Transformation De L'Économie Française
----------------------------------------------------------------------

- https://www.librairiesindependantes.com/product/9782738154262/

::

    EAN13
        9782738154262
    Éditeur
        Odile Jacob
    Date de publication
        26 janvier 2022
    Collection
        OJ.ECONOMIE
    Nombre de pages
        256
    Dimensions
        22,1 x 14,6 x 1,7 cm
    Poids
        390 g
    Langue
        fre

« La question que nous nous sommes posée peut se résumer ainsi : que faut-il
faire pour mettre l’économie française en cohérence avec une baisse des émissions
planétaires de 5 % par an, compatible avec nos engagements climatiques, tout
en permettant à chacun(e) de trouver un emploi ?

C’est ce plan de marche visant la décarbonation effective de nos activités
que nous avons essayé de construire.

Derrière les chapitres qui suivent, il y a l’apport de dizaines de collaborateurs,
de centaines de contributeurs et de milliers de relecteurs.

Il a fallu en défricher des sujets pour commencer à avoir une vue d’ensemble !

Si ce plan parvient à faire un tant soit peu la différence dans les débats
à venir, nous n’aurons pas perdu notre temps. » Jean-Marc Jancovici

Réhabiter Le Monde, Pour Une Politique Des Biorégions par Agnès Sinaï
-------------------------------------------------------------------------

- https://www.librairiesindependantes.com/product/9782021459296/

::

    AN13
        9782021459296
    Éditeur
        Seuil
    Date de publication
        27 octobre 2023
    Collection
        Anthropocène
    Nombre de pages
        320
    Dimensions
        19 x 14 x 2,3 cm
    Poids
        358 g
    Langue
        fre

La métropolisation du monde a bouleversé les paysages.

Les villes sont désormais géantes et leur étalement sans fin.

Nos existences se déroulent dans ces cités irriguées de réseaux invisibles
d’acheminement d’eau, d’électricité et de nourriture.

Mais depuis ces îlots de production et de consommation, que reste-t-il de nos
liens avec le vivant ?

Et si l’horizon bitumé n’était pas le seul futur possible ?

L’urgence sociale et écologique nous enjoint de mobiliser de nouvelles échelles
d’existence afin d’inventer d’autres formes de sociétés en commun : réhabiter le monde.

Pour insuffler un nouvel imaginaire et réparer les liens et les lieux, ce
livre explore la vision biorégionale, élaborée il y a un demi-siècle dans le
creuset de la contre-culture californienne.

Abordé ici dans sa dimension cosmopolitique et permaculturelle, le biorégionalisme
se fonde sur la nécessité d’entrer dans un rapport de résonance avec la terre :
savoir où et de quoi l’on vit et ce qui vibre sous nos pas.

Réhabiter la terre, c’est se penser comme hôte cohabitant et non comme
propriétaire, dans des sociétés écologiques ouvertes.

À travers un exercice prospectif appliqué à l’Île-de-France se déploie un
nouvel imaginaire de la métropole.

En 2050, le territoire francilien est désormais éclaté en huit biorégions
quasi autonomes sur le plan alimentaire à base de low tech et à échelle humaine.

Engagé dans une vision émancipatrice, l’ouvrage invite à une remobilisation
politique.

Agnès Sinaï est journaliste, essayiste et dirige l’Institut Momentum qu’elle
a co-fondé en 2011.
Docteure en aménagement de l’espace et urbanisme, elle a dirigé les trois
tomes des Politiques de l’Anthropocène (Presses de Sciences Po).

Depuis 2010, elle enseigne à Sciences Po dans le cadre du cours sur les
politiques de la décroissance.



Antoine Back
===============

- https://x.com/abkgrenoble
- https://x.com/abkgrenoble/status/1800679199063069129


Elu #Grenoble Risques, résilience territoriale
Prospective, évaluation, nvx indicateurs
Monde académique Stratégie alimentaire
@Grenoble_Commun ★@Mouvt_ENSEMBLE


.. figure:: images/20240611_225053_800_antoine.webp

.. figure:: images/antoine.webp

   https://x.com/abkgrenoble/status/1800679199063069129

Cultiver les espoirs lucides, embrasser la catastrophe imminente et les moyens
de la conjurer. Une conférence d'ArthurKeller c'est un peu comme un album
de Stupeflip : ça t'prend par la croupe, ça t'retourne comme une crêpe,
ça t'agrippe, ça t'attrape, ça fait pas d'sentiment


Dessins de Jalan
==================


.. figure:: images/20240611_225916_800_limites_planetaires.webp
.. figure:: images/20240611_225932_800_preparer.webp
.. figure:: images/20240611_230005_800_reagir.webp
.. figure:: images/20240611_230013_800_imaginer_nouveaux_recits.webp



Le RARRe (Rapport Annuel sur les Risques et la Résilience) premier rapport annuel sur les risques et la résilience dans l’aire grenobloise
================================================================================================================================================

- https://www.aurg.fr/11226-rarre.htm
- https://www.aurg.fr/article/486/2205-parution-du-premier-rapport-annuel-sur-les-risques-et-la-resilience-rarre-dans-l-aire-grenobloise.htm

`Le rapport au format PDF <https://basedoc.aurg.fr/dyn/portal/digidoc.xhtml?statelessToken=Sy2j6HJcpDzjOTF_AdNGsR4DTrR27-ix4XX6Z8PdYo4=&actionMethod=dyn%2Fportal%2Fdigidoc.xhtml%3AdownloadAttachment.openStateless>`_

.. figure:: images/le_rarre.webp


Cette publication est la première édition du rapport annuel sur les risques
et la résilience dans l'aire grenobloise : le RARRe 2024

Une démarche inédite à cette échelle, fruit du travail engagé au sein de
l’Atelier des Futurs, collectif des acteurs locaux de la prospective.

Son objectif est d’éclairer le débat public sur ces sujets sensibles et complexes,
de faire comprendre la dimension systémique et multifactorielle des risques
et in fine, d’aider les acteurs publics à la décision.

La méthode exploratoire est directement inspirée du Global Risks Report porté
par le Forum économique Mondial (Forum de Davos).

Elle repose sur l’identification d’une liste de risques et vulnérabilités,
qu’elle s’attache à documenter (objectivation) et à questionner dans le
cadre d’une enquête (perception).

Le Rapport final est une mise en perspective des deux complétée des enseignements
du Forum du 6 février 2024 qui a réuni une majorité d’élus locaux.

Il a été rédigé par l’Agence d’urbanisme de la région grenobloise, à l’initiative
et cheville ouvrière du projet.

Nous attirons l’attention sur le fait qu’il s’agit d’une version expérimentale,
de fait lacunaire et imparfaite, appelée à s’améliorer dans le temps puisque
le RARRe sera reconduit et enrichi annuellement.

Le dernier document est une synthèse des enseignements.

D’autres publications, notamment les 6 livrets-risques qui rassemblent la
documentation, sont également accessibles sur le site Internet de l’Agence

- `(onglet Atelier des Futurs / Le RARRe) <https://www.aurg.fr/11226-rarre.htm>`_
- `et sur cette base documentaire https://basedoc.aurg.fr/dyn/portal/index.xhtml?page=alo&aloId=13213&espaceId=50 <https://basedoc.aurg.fr/dyn/portal/index.xhtml?page=alo&aloId=13213&espaceId=50>`_


Message sur mastodon
========================

- https://kolektiva.social/@grenobleluttes/112613596045791402

Sur twitter
=============

- https://x.com/VilledeGrenoble/status/1801534634028622217


Sur Gremag le 27 juin 2024
================================

- https://www.gremag.fr/article/1034/15-retour-sur-la-venue-d-arthur-keller-a-l-hotel-de-ville-de-grenoble.htm

podcast
----------

- https://podcast.ausha.co/grenoble-2040/arthur-keller-le-monde-de-demain-se-preparer-des-aujourd-hui-ensemble
