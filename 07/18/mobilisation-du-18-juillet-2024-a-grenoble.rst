

.. _grenoble_2024_07_18:

================================================================
2024-07-18 **Mobilisation du 18 juillet 2024 à Grenoble**
================================================================

- https://www.ldh-france.org/mobilisation-18-juillet-gagner-un-changement-democratique-et-social/
- https://www.youtube.com/watch?v=FbbbDk6C7RQ

.. youtube:: FbbbDk6C7RQ


.. figure:: images/20240718_192625_800.webp  

.. figure:: images/20240718_193936_800.webp


Appel à l’initiative de la LDH, d’Attac et la CGT. 
Mobilisations le 18 juillet 2024, à partir de 12h place de la République 
à Paris et partout en France

A la suite de la dissolution de l’Assemblée nationale, la société civile
s’est mobilisée de manière à empêcher l’accession au pouvoir de l’extrême
droite. Elle a accueilli avec un soulagement profond les désistements républicains
qu’elle appelait de ses vœux et le succès, certes relatif, des forces
démocratiques. Depuis lors, le président de la République n’a eu de cesse de
contester la victoire du Nouveau Front Populaire arrivé en tête aux élections
législatives anticipées. À l’inverse du respect de l’usage démocratique,
il a exprimé sa volonté d’imposer une coalition dont son ex-majorité serait
le pivot, et refusé d’envisager la formation d’un gouvernement conforme au
choix des électrices et électeurs.

Une telle issue ne ferait qu’accentuer le chaos démocratique et social en niant
le verdict des urnes. En tant que protecteur de “l’intérêt supérieur de
la Nation et garant des institutions”, le Président est tenu de respecter le
résultat du vote. Nous, organisations syndicales, associatives et collectifs,
qui avons décidé de prendre une part active à la destinée institutionnelle et
sociale du pays, comme cela avait toujours été le cas avant le mépris affiché
d’Emmanuel Macron pour les corps intermédiaires, constatons qu’une fois
encore ce dernier rechigne à respecter les règles.

Ces élections ont marqué le désaveu des politiques néolibérales menées avec
brutalité par Emmanuel Macron en faveur des plus fortunés et des dirigeants des
grandes entreprises, au détriment des travailleurs et travailleuses.
 
**Le programme néolibéral, de destruction des services publics, de sape des libertés publiques,
d’atteintes à l’Etat de droit et d’inaction en matière environnementale a
conduit inexorablement à l’instabilité et à l’exaspération sociale**. 

Ces politiques économiques antisociales sont le carburant du RN. Les votes ont
également exprimé le refus de l’extrême droite – et son lot de racisme,
d’antisémitisme, d’islamophobie, de sexisme, de LGBTQIA+phobie, et de toutes
les discriminations.

La mise en œuvre d’une politique sociale, écologique, féministe et
antiraciste est la seule porte de sortie par le haut de cette crise politique et
institutionnelle. En donnant un plus grand nombre de député-e-s aux groupes du
Nouveau Front Populaire, les urnes ont exprimé l’aspiration de la population
à la mise en œuvre de mesures qui rompent avec des politiques injustes et
inégalitaires.

C’est la responsabilité des forces progressistes de rester unies pour
porter et incarner cette alternative. 
Après une mobilisation historique où le mouvement social a pris toute sa part, 
elles ne doivent pas faillir et proposer un gouvernement pour mettre en 
œuvre des mesures d’urgence : augmentation des salaires, abrogation des 
réformes des retraites et de l’assurance chômage, retrait de la loi immigration, 
adoption d’une loi cadre sur les violences sexistes et sexuelles, réponse 
à l’urgence environnementale… 

**La seule solution, c’est une réelle alternative démocratique et sociale !**

Pour faire entendre cette exigence pour le progrès social et la démocratie,
nous appelons à rejoindre le rassemblement organisé le 18 juillet 2024 à Paris à
partir de 12h place de la République.

#NousFaisonsPressionPopulaire
