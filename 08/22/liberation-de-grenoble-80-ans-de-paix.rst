

.. _grenoble_2024_08_22:

===========================================================
2024-08-22 Libération de Grenoble : 80 ans de paix
===========================================================

- https://www.gremag.fr/article/1029/17-liberation-de-grenoble-80-ans-de-paix.htm

Ce 22 août 2024, Grenoble célèbre les 80 ans de sa
Libération. L’occasion pour la Ville de rappeler auprès du grand public
les événements qui ont mené vers cette victoire de la liberté et de la
paix, à un moment clé de l’histoire contemporaine française.

Par décret du 4 mai 1944, Grenoble était honorée du titre de "Compagnon
de La Libération". Une distinction qui récompensait les actions héroïques
de la Résistance locale contre l’occupant nazi et rappelait en même temps
le lourd tribut payé par ces femmes et ces hommes. Tant de vies sacrifiées
contribueront à la déroute de l’ennemi et à la Libération de la Ville
le 22 août 1944, avec l’arrivée triomphale à Grenoble des maquisards,
des Groupes Francs et du 1er bataillon de choc, rejoints par les soldats
américains du 143e régiment d’infanterie.

Quelques mois plus tard, le 5 novembre 1944, le général de Gaulle, alors
chef du Gouvernement provisoire de la République française, venait saluer
officiellement l’engagement de Grenoble. Il remettait à notre ville
la croix de la Libération, sur la place Pasteur où furent parqués le 11
novembre 1943 des milliers de Grenoblois et de Grenobloises qui manifestaient
contre la répression.
