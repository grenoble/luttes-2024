# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# http://www.sphinx-doc.org/en/master/config
# -- Path setup --------------------------------------------------------------
# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))
import platform
from datetime import datetime
from zoneinfo import ZoneInfo

import sphinx
import sphinx_material

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
# sys.path.insert(0, os.path.abspath("./src"))

project = "Grenoble luttes 2024"
html_title = project

author = f"Human people"
# html_logo = "images/mahsa_jina_amini_avatar.png"
# html_favicon = "images/mahsa_jina_amini_avatar.png"
release = "0.1.0"
now = datetime.now(tz=ZoneInfo("Europe/Paris"))
version = f"{now.year}-{now.month:02}-{now.day:02} {now.hour:02}H ({now.tzinfo})"
today = version

extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.extlinks",
    "sphinx.ext.intersphinx",
    "sphinx.ext.todo",
    "sphinx_copybutton",
]
autosummary_generate = True
autoclass_content = "class"

# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store"]
html_static_path = ["_static"]
html_show_sourcelink = True
html_sidebars = {
    "**": ["logo-text.html", "globaltoc.html", "localtoc.html", "searchbox.html"]
}
extensions.append("sphinx_material")
html_theme_path = sphinx_material.html_theme_path()
html_context = sphinx_material.get_html_context()
html_theme = "sphinx_material"

extensions.append("sphinx_design")
extensions.append("sphinx.ext.intersphinx")
intersphinx_mapping = {
    "grenoble_infos": ("https://grenoble.frama.io/infos/", None),
    "iran_luttes": ("https://iran.frama.io/luttes/", None),
    "iran_2024": ("https://iran.frama.io/luttes-2024/", None),
    "illiberalisme": ("https://luttes.frama.io/contre/l-illiberalisme/", None),
    "antisemitisme": ("https://luttes.frama.io/contre/l-antisemitisme/", None),
    "violences": ("https://luttes.frama.io/contre/les-violences-policieres/", None),
    # "antifa": ("https://luttes.frama.io/contre/le-fascisme/", None),
    "media_2023": ("https://luttes.frama.io/media-2023/", None),
    "osm_2023": ("https://luttes.frama.io/pour/le-libre/opendata/openstreetmap-2023/", None),
    "osm": ("https://luttes.frama.io/pour/le-libre/opendata/openstreetmap", None),
}

extensions.append("sphinx.ext.todo")
todo_include_todos = True
# https://sphinx-design.readthedocs.io/en/furo-theme/get_started.html
extensions.append("sphinx_design")

# material theme options (see theme.conf for more information)
# https://gitlab.com/bashtage/sphinx-material/blob/master/sphinx_material/sphinx_material/theme.conf
# Colors
# The theme color for mobile browsers. Hex color.
# theme_color = #3f51b5
# Primary colors:
# red, pink, purple, deep-purple, indigo, blue, light-blue, cyan,
# teal, green, light-green, lime, yellow, amber, orange, deep-orange,
# brown, grey, blue-grey, white
# Accent colors:
# red, pink, purple, deep-purple, indigo, blue, light-blue, cyan,
# teal, green, light-green, lime, yellow, amber, orange, deep-orange
# color_accent = blue
# color_primary = blue-grey

# material theme options (see theme.conf for more information)
html_theme_options = {
    "base_url": "https://grenoble.frama.io/luttes-2024/",
    "repo_url": "https://framagit.org/grenoble/luttes-2024",
    "repo_name": project,
    "html_minify": False,
    "html_prettify": True,
    "css_minify": True,
    "repo_type": "gitlab",
    "globaltoc_depth": -1,
    "color_primary": "green",
    "color_accent": "cyan",
    "theme_color": "#2196f3",
    "nav_title": f"{project} ({today})",
    "master_doc": False,
    "nav_links": [
        {
            "href": "genindex",
            "internal": True,
            "title": "Index",
        },
        {
            "href": "https://grenoble.frama.io/infos/index.html",
            "internal": False,
            "title": "Infos Grenoble",
        },
        {
            "href": "https://grenoble.frama.io/linkertree/",
            "internal": False,
            "title": "Liens Grenobleluttes",
        },
        {
            "href": "https://luttes.frama.io/contre/les-violences-policieres",
            "internal": False,
            "title": "Contre les violences policières",
        },
        {
            "href": "https://luttes.frama.io/contre/l-antisemitisme/",
            "internal": False,
            "title": "Contre l'antisémitisme",
        },
        {
            "href": "https://antiracisme.frama.io/linkertree/",
            "internal": False,
            "title": "Liens antiracisme",
        },
    ],
    "heroes": {
        "index": "Grenoble luttes 2024",
    },
    "table_classes": ["plain"],
}
# https://github.com/sphinx-contrib/yasfb
extensions.append("yasfb")
feed_base_url = html_theme_options["base_url"]
feed_author = "Scribe"
# https://sphinx-design.readthedocs.io/en/furo-theme/get_started.html
extensions.append("sphinx_design")
# https://sphinx-tags.readthedocs.io/en/latest/quickstart.html#installation
extensions.append("sphinx_tags")
tags_create_tags = True
# Whether to display tags using sphinx-design badges.
tags_create_badges = True
extensions.append("sphinxcontrib.youtube")


language = "en"
html_last_updated_fmt = ""

todo_include_todos = True

html_use_index = True
html_domain_indices = True


copyright = f"-3000-{now.year}, {author} Built with sphinx {sphinx.__version__} Python {platform.python_version()}"

rst_prolog = """
.. |ici_grenoble| image:: /images/ici_grenoble_avatar.png
.. |tamis| image:: /images/tamis_avatar.png
.. |cric| image:: /images/cric_avatar.png
.. |solidarite| image:: /images/solidarite_avatar_32.png
.. |solidarite_ukraine| image:: /images/solidarite_ukraine_avatar.png
.. |macron| image:: /images/macron_avatar.png
.. |car38| image:: /images/car38_avatar.png
.. |jo| image:: /images/jo_casseroles_avatar.png
.. |casserolade| image:: /images/casserolade_avatar.png
.. |JinaAmini| image:: /images/mahsa_jina_amini_avatar.png
.. |important| image:: /images/important_ici.png
.. |anar| image:: /images/anar_avatar.png
.. |rebellion| image:: /images/extinction_rebellion.png
.. |alternatiba| image:: /images/alternatiba_avatar.png
.. |medics| image:: /images/street_medics_avatar.png
.. |afrique| image:: /images/afrique_avatar.png
.. |cram| image:: /images/cram_logo.png
.. |mriya| image:: /images/mriya_avatar.png
.. |antigone| image:: /images/antigone_avatar.png
.. |crha| image:: /images/crha_avatar.png
.. |sdterre| image:: /images/sdt_avatar.png
.. |communisme_libertaire| image:: /images/rouge_et_noir.png
.. |osm| image:: /images/osm_avatar.png
.. |sotm| image:: /images/sotm_avatar.png
.. |panoramax| image:: /images/panoramax_avatar.png
.. |cisem| image:: /images/cisem_avatar.png
.. |FluxWeb| image:: /images/rss_avatar.webp
"""
