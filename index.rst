
.. raw:: html

   <a rel="me" href="https://kolektiva.social/@grenobleluttes"></a>


.. ♀️✊ ⚖️ 📣 🙏
.. ✊🏻✊🏼✊🏽✊🏾✊🏿
.. 🤥
.. 🤪
.. ⚖️ 👨‍🎓
.. 🌍 ♀️✊🏽
.. 🇮🇷
.. 🎥 🎦
.. ⏚

.. un·e

|FluxWeb| `RSS <https://grenoble.frama.io/luttes-2024/rss.xml>`_

.. _luttes_grenoble_2024:

=========================================
**Luttes Grenoble 2024**
=========================================

- :ref:`grenoble_infos:agendas_grenoble`

#Grenoble #Solidarite #SolidaritéInternationale #Migrants
#PersonneNestIllegal #SolidariteAntiraciste SolidariteAntifasciste #SolidaritéInternationale #ContreLaLoiDarmanin
#DesobeissanceCivile #CISEM #14Janvier #14Janvier 2024 #21Janvier  #21Janvier2024
#RetraitLoiImmigration #Solidarite

.. tags:: #SolidaritéInternationale, Solidarite, Migrants

.. toctree::
   :maxdepth: 6

   08/08
   07/07
   06/06
   05/05
   03/03
   01/01
